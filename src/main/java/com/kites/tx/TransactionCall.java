package com.kites.tx;

import javax.persistence.EntityManager;

public interface TransactionCall<T> {
    T executeTransaction(EntityManager entityManager);
}
