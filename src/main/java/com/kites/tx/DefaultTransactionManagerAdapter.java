package com.kites.tx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

public class DefaultTransactionManagerAdapter implements TransactionManagerAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTransactionManagerAdapter.class);
    private EntityManagerFactory entityManagerFactory;
    private Boolean completed;

    public DefaultTransactionManagerAdapter(
            EntityManagerFactory entityManagerFactory) {
        assert (entityManagerFactory != null);
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public <T> T doTransaction(TransactionCall<T> transactionCall) throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = beginTransaction(entityManager);
        try {
            T t = transactionCall.executeTransaction(entityManager);
            commit(transaction);
            return t;
        } catch (Exception e) {
            rollback(transaction);
            throw e;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    private EntityTransaction beginTransaction(EntityManager entityManager) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        return transaction;
    }

    private void commit(EntityTransaction transaction) {
        transaction.commit();
        completed = true;
    }

    private void rollback(EntityTransaction transaction) {
        transaction.rollback();
        completed = false;
    }

    @Override
    public boolean isTransactionComplete() {
        return completed;
    }

    @Override
    public boolean isTransactionRollBack() {
        return !completed;
    }
}
