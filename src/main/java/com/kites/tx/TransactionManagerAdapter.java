package com.kites.tx;

import javax.persistence.EntityManager;

public interface TransactionManagerAdapter {
    <T> T doTransaction(TransactionCall<T> transactionCall) throws Exception;
    EntityManager getEntityManager();
    boolean isTransactionComplete();
    boolean isTransactionRollBack();
}
