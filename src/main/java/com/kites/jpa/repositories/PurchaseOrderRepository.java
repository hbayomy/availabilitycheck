package com.kites.jpa.repositories;

import com.kites.jpa.entities.tr.po.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PurchaseOrderRepository extends  JpaRepository<PurchaseOrder,Long> {

    @Query("SELECT P.code FROM PurchaseOrder P WHERE P.id = :maxId AND SUBSTRING(P.code,0,4) = :year")
    Optional<String> findPurchaseOrderCodeWithMaxIdAndStartsWithYear(Long maxId,String year);

    @Query("SELECT MAX(P.id) FROM PurchaseOrder P ")
    Optional<Long> findMaxId();
}
