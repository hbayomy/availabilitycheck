package com.kites.jpa.entities.tr;

import com.kites.jpa.entities.BusinessObject;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class TransactionEntity extends BusinessObject {

    @Column(nullable = false)
    private String currentStates;

    public String getCurrentStates() {
        return currentStates;
    }

    public void setCurrentStates(String currentStates) {
        this.currentStates = currentStates;
    }

    @Override
    public String toString(){
        StringBuilder transactionStringBuilder =
                new StringBuilder(super.toString());
        transactionStringBuilder
                .append(currentStates.toString())
                .append(",");
        return transactionStringBuilder.toString();
    }

}
