package com.kites.jpa.entities.tr.po;

import com.kites.jpa.entities.tr.TransactionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class PurchaseOrder extends TransactionEntity {

    @Column(nullable = false)
    private PurchaseOrderType orderType;
    @Column(nullable = false)
    private String version;
    @Column(nullable = false)
    private Long purchaseUnitId;
    @Column(nullable = false)
    private Long vendorId;
    @Column(nullable = false)
    private Long purchaseResponsibleId;
    @OneToMany(cascade = CascadeType.ALL)
    private List<PurchaseOrderItem> orderItems = new ArrayList<>();

    public PurchaseOrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(PurchaseOrderType orderType) {
        this.orderType = orderType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Long getPurchaseUnitId() {
        return purchaseUnitId;
    }

    public void setPurchaseUnitId(Long purchaseUnitId) {
        this.purchaseUnitId = purchaseUnitId;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public Long getPurchaseResponsibleId() {
        return purchaseResponsibleId;
    }

    public void setPurchaseResponsibleId(Long purchaseResponsibleId) {
        this.purchaseResponsibleId = purchaseResponsibleId;
    }

    public List<PurchaseOrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<PurchaseOrderItem> orderItems){
        this.orderItems = orderItems;
    }

    @Override
    public String toString(){
        StringBuilder purchaseOrderStringBuilder
                = new StringBuilder(super.toString());
        purchaseOrderStringBuilder
                .append(String.valueOf(orderType))
                .append(",")
                .append(String.valueOf(version))
                .append(",")
                .append(String.valueOf(vendorId))
                .append(",")
                .append(String.valueOf(purchaseUnitId))
                .append(",")
                .append(String.valueOf(purchaseResponsibleId))
                .append(",")
                .append(orderItems==null?"[]":orderItems.toString())
                .append("}");
        return purchaseOrderStringBuilder.toString();
    }
}
