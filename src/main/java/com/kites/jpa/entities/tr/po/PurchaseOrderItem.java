package com.kites.jpa.entities.tr.po;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class PurchaseOrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long purchaseOrderId;
    private Long itemId;
    private Double quantityInDLPL;
    @OneToMany(cascade = CascadeType.ALL)
    private List<PurchaseOrderItemQuantity> itemQuantities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(Long purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Double getQuantityInDLPL() {
        return quantityInDLPL;
    }

    public void setQuantityInDLPL(Double quantityInDLPL) {
        this.quantityInDLPL = quantityInDLPL;
    }

    public List<PurchaseOrderItemQuantity> getItemQuantities() {
        return itemQuantities;
    }

    public void setItemQuantities(List<PurchaseOrderItemQuantity> itemQuantities) {
        this.itemQuantities = itemQuantities;
    }

    @Override
    public boolean equals(Object purchaseOrderItem){
        return id.equals(((PurchaseOrderItem)purchaseOrderItem).getId())
                && (
                        (itemQuantities==null && ((PurchaseOrderItem) purchaseOrderItem).getItemQuantities()==null)
                        ||
                        (itemQuantities.equals(((PurchaseOrderItem)purchaseOrderItem).getItemQuantities()))
                );
    }

    @Override
    public String toString() {
        StringBuilder purchaseOrderItemStringBuilder = new StringBuilder();
        purchaseOrderItemStringBuilder
                .append("OrderItem{")
                .append(String.valueOf(itemId))
                .append(",")
                .append(String.valueOf(quantityInDLPL))
                .append(",")
                .append(itemQuantities==null?"[]":itemQuantities.toString())
                .append("}");
        return purchaseOrderItemStringBuilder.toString();
    }
}
