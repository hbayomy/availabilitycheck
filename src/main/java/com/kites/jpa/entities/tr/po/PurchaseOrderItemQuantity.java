package com.kites.jpa.entities.tr.po;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PurchaseOrderItemQuantity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long orderItemId;
    private Long unitOfMeasureId;
    private Double quantity;
    private Double price;
    private Double discount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Long getUnitOfMeasureId() {
        return unitOfMeasureId;
    }

    public void setUnitOfMeasureId(Long unitOfMeasureId) {
        this.unitOfMeasureId = unitOfMeasureId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object purchaseOrderItemQuantity){
        return id.equals(((PurchaseOrderItemQuantity)purchaseOrderItemQuantity).getId());
    }

    @Override
    public String toString() {
        StringBuilder purchaseOrderItemStringBuilder = new StringBuilder();
        purchaseOrderItemStringBuilder
                .append("Quantity{")
                .append(String.valueOf(unitOfMeasureId))
                .append(",")
                .append(String.valueOf(quantity))
                .append(",")
                .append(String.valueOf(price))
                .append(",")
                .append(String.valueOf(discount))
                .append(",")
                .append("}");
        return purchaseOrderItemStringBuilder.toString();
    }

}
