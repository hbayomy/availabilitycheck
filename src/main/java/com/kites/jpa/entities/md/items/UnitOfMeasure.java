package com.kites.jpa.entities.md.items;

import com.kites.jpa.entities.BusinessObject;

import javax.persistence.Entity;

@Entity
public class UnitOfMeasure extends BusinessObject {
    private String symbol;
    private String description;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
