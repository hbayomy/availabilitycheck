package com.kites.jpa.entities.md;

import com.kites.jpa.entities.BusinessObject;

import javax.persistence.Entity;

@Entity
public class ItemVendorRecord extends BusinessObject {
    private Long ItemId;
    private Long vendorId;
    private Long unitOfMeasureId;
    private Long purchaseUnitId;

    public Long getItemId() {
        return ItemId;
    }

    public void setItemId(Long itemId) {
        ItemId = itemId;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public Long getUnitOfMeasureId() {
        return unitOfMeasureId;
    }

    public void setUnitOfMeasureId(Long unitOfMeasureId) {
        this.unitOfMeasureId = unitOfMeasureId;
    }

    public Long getPurchaseUnitId() {
        return purchaseUnitId;
    }

    public void setPurchaseUnitId(Long purchaseUnitId) {
        this.purchaseUnitId = purchaseUnitId;
    }
}
