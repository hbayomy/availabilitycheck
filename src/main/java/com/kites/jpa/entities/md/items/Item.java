package com.kites.jpa.entities.md.items;

import com.kites.jpa.entities.md.MasterDataEntity;

import javax.persistence.Entity;

@Entity
public class Item extends MasterDataEntity {

    public void setItemGroupId(Long itemGroupId) {
    }

    public void setBaseUnitId(Long baseUnitId) {
    }

    @Override
    public String toString() {
        StringBuilder itemStringBuilder
                = new StringBuilder(super.toString());
        itemStringBuilder
                .append("}");
        return itemStringBuilder.toString();
    }

}
