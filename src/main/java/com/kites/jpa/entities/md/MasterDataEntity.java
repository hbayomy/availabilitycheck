package com.kites.jpa.entities.md;

import com.kites.jpa.entities.BusinessObject;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class MasterDataEntity extends BusinessObject {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder masterDataStringBuilder
                = new StringBuilder(super.toString());
        masterDataStringBuilder
                .append(String.valueOf(name))
                .append(",");
        return masterDataStringBuilder.toString();
    }

}
