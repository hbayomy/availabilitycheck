package com.kites.jpa.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@MappedSuperclass
public abstract class BusinessObject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Timestamp creationDate;
    @Column(nullable = false)
    private String creationInfo;
    @Column(nullable = false)
    private Timestamp modificationDate;
    @Column(nullable = false)
    private String modificationInfo;
    @Column(nullable = false, unique = true)
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationInfo() {
        return creationInfo;
    }

    public void setCreationInfo(String creationInfo) {
        this.creationInfo = creationInfo;
    }

    public Timestamp getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Timestamp modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModificationInfo() {
        return modificationInfo;
    }

    public void setModificationInfo(String modificationInfo) {
        this.modificationInfo = modificationInfo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        StringBuilder businessObjectStringBuilder
                = new StringBuilder();
        businessObjectStringBuilder
                .append("\n")
                .append(getClass().getSimpleName())
                .append("{")
                .append(String.valueOf(id))
                .append(",")
                .append(String.valueOf(creationDate))
                .append(",")
                .append(String.valueOf(creationInfo))
                .append(",")
                .append(String.valueOf(modificationDate))
                .append(",")
                .append(String.valueOf(modificationInfo))
                .append(",")
                .append(String.valueOf(code))
                .append(",")
        ;
        return businessObjectStringBuilder.toString();
    }

    @Override
    public boolean equals(Object businessObject){
        return getId().equals(((BusinessObject)businessObject).getId());
    }
}
