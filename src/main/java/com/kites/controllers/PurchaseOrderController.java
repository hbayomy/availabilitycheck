package com.kites.controllers;

import com.google.gson.Gson;
import com.kites.businesslogic.commands.PurchaseOrderCreateCommand;
import com.kites.businesslogic.commands.PurchaseOrderCreationData;
import com.kites.tx.DefaultTransactionManagerAdapter;
import com.kites.tx.TransactionManagerAdapter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;

@RestController
@RequestMapping(value = "purchase-order/")
public class PurchaseOrderController implements InitializingBean {

    @Autowired
    private EntityManagerFactory entityManagerFactory;
    private TransactionManagerAdapter transactionManagerAdapter;

    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public @ResponseBody
    String createImportPurchaseOrder(@RequestBody String orderCreationData)
            throws Exception {
        PurchaseOrderCreationData data = new Gson().fromJson(orderCreationData,PurchaseOrderCreationData.class);
        PurchaseOrderCreateCommand command = new PurchaseOrderCreateCommand(transactionManagerAdapter);
        command.execute(data);
        return "The Purchase Order is created successfully";
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        transactionManagerAdapter = new DefaultTransactionManagerAdapter(entityManagerFactory);
    }
}
