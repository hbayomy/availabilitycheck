package com.kites.businesslogic.commands;

import com.kites.jpa.entities.tr.po.PurchaseOrder;
import com.kites.jpa.entities.tr.po.PurchaseOrderType;
import com.kites.businesslogic.services.CodeGenerationService;
import com.kites.tx.TransactionManagerAdapter;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class PurchaseOrderCreateCommand {

    private TransactionManagerAdapter transactionManager;
    private CodeGenerationService codeGenerator;

    public PurchaseOrderCreateCommand(TransactionManagerAdapter transactionManagerAdapter) {
        this.transactionManager = transactionManagerAdapter;
        assert (transactionManager != null);
        this.codeGenerator = new CodeGenerationService(transactionManager);

    }

    public PurchaseOrder execute(PurchaseOrderCreationData poData) throws Exception {
        PurchaseOrder po = createPurchaseOrder(poData.getOrderType(),poData.getVendorId(),
                poData.getPurchaseUnitId(),poData.getPurchaseResponsibleId());
        return po;
    }

    private PurchaseOrder createPurchaseOrder(PurchaseOrderType orderType, Long vendorId, Long purchaseUnitId,
                                             Long purchaseResponsibleId) throws Exception {
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        LocalDateTime time = LocalDateTime.now();
        purchaseOrder.setCreationDate(Timestamp.valueOf(time));
        purchaseOrder.setCreationInfo("Hi, PO is created");
        purchaseOrder.setModificationDate(Timestamp.valueOf(time));
        purchaseOrder.setModificationInfo("Hi, PO is created");
        purchaseOrder.setCurrentStates("[Draft]");
        purchaseOrder.setOrderType(orderType);
        purchaseOrder.setVendorId(vendorId);
        purchaseOrder.setPurchaseUnitId(purchaseUnitId);
        purchaseOrder.setPurchaseResponsibleId(purchaseResponsibleId);
        purchaseOrder.setCode(codeGenerator.generateYearSerialCode(PurchaseOrder.class,time));
        purchaseOrder.setVersion("v1.0");
        return transactionManager.doTransaction((EntityManager entityManager) -> {
            entityManager.persist(purchaseOrder);
            return purchaseOrder;
        });
    }

}
