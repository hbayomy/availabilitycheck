package com.kites.businesslogic.commands;

import com.kites.jpa.entities.tr.po.PurchaseOrderType;

public class PurchaseOrderCreationData {

    private PurchaseOrderType orderType;
    private Long vendorId;
    private Long purchaseUnitId;
    private Long purchaseResponsibleId;

    public PurchaseOrderCreationData(String orderType, Long vendorId, Long purchaseUnitId,
                                     Long purchaseResponsibleId) {
        this.orderType = PurchaseOrderType.valueOf(orderType);
        this.vendorId = vendorId;
        this.purchaseUnitId = purchaseUnitId;
        this.purchaseResponsibleId = purchaseResponsibleId;
    }

    public PurchaseOrderType getOrderType() {
        return orderType;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public Long getPurchaseUnitId() {
        return purchaseUnitId;
    }

    public Long getPurchaseResponsibleId() {
        return purchaseResponsibleId;
    }
}
