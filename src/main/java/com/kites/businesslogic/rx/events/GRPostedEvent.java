package com.kites.businesslogic.rx.events;

import com.kites.jpa.entities.tr.gr.GoodsReciept;
import com.kites.businesslogic.rx.BusinessObjectEvent;

public class GRPostedEvent extends BusinessObjectEvent<GoodsReciept> {

    private Long goodsRecieptID;
    private String goodsRecieptCode;
    private Double totalCost;
    private Long itemId;
    private String itemCode;
    private String stockType;
    private Long measureId;
    private String measureCode;

    public GRPostedEvent(){
        super(GoodsReciept.class);
    }

    public GRPostedEvent(Long goodsRecieptID, String goodsRecieptCode, Double totalCost, Long itemId, String itemCode,
                         String stockType, Long measureId, String measureCode) {
        super(GoodsReciept.class);
        this.goodsRecieptID = goodsRecieptID;
        this.goodsRecieptCode = goodsRecieptCode;
        this.itemId = itemId;
        this.itemCode = itemCode;
        this.totalCost = totalCost;
        this.stockType = stockType;
        this.measureId = measureId;
        this.measureCode = measureCode;
    }

    @Override
    public boolean equals(Object obj){

        return this.goodsRecieptCode.equals(
                ((GRPostedEvent)obj).goodsRecieptCode
        );
    }
}
