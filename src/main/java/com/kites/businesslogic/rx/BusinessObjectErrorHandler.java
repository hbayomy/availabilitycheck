package com.kites.businesslogic.rx;

import io.reactivex.functions.Consumer;

public interface BusinessObjectErrorHandler extends Consumer<Throwable> {

    @Override
    void accept(Throwable throwable) throws Exception;
}
