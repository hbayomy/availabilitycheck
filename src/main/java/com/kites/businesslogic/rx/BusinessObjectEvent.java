package com.kites.businesslogic.rx;

import com.kites.jpa.entities.BusinessObject;

public class BusinessObjectEvent<T extends BusinessObject> {
    private Class<T> businessObjectClass;
    public BusinessObjectEvent(Class<T> businessObjectClass){
        this.businessObjectClass = businessObjectClass;
    }

    public Class<T> getBusinessObjectClass() {
        return this.businessObjectClass;
    }
}
