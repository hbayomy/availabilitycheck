package com.kites.businesslogic.rx;

import com.kites.jpa.entities.BusinessObject;
import io.reactivex.subjects.PublishSubject;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class BusinessObjectChannel {

    private static BusinessObjectChannel instance;
    private static final
    ConcurrentMap<Class<? extends BusinessObject>, PublishSubject<? extends BusinessObjectEvent>>
            observables = new ConcurrentHashMap<>();


    private BusinessObjectChannel() {

    }

    public synchronized static BusinessObjectChannel getInstance() {
        if (instance == null) instance = new BusinessObjectChannel();
        return instance;
    }

    public synchronized <T extends BusinessObject,E extends BusinessObjectEvent> void
    subscribe(BusinessObjectEventHandler eventHandler, BusinessObjectErrorHandler errorHandler, Class<T> businessObjectClass) {
        observables.putIfAbsent(businessObjectClass, PublishSubject.<E>create());
        PublishSubject<E> observable = (PublishSubject<E>) observables.get(businessObjectClass);
        observable.subscribe(eventHandler,errorHandler);
    }

    public synchronized <T extends BusinessObjectEvent> void publish(T event) {
        PublishSubject<T> observable = (PublishSubject<T>) observables.getOrDefault(event.getBusinessObjectClass()
                ,PublishSubject.<T>create());
        observable.onNext(event);
    }

}
