package com.kites.businesslogic.rx;

import io.reactivex.functions.Consumer;

public interface BusinessObjectEventHandler extends Consumer<BusinessObjectEvent> {

    @Override
    void accept(BusinessObjectEvent e) throws Exception;
}
