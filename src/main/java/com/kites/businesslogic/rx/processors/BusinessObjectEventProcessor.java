package com.kites.businesslogic.rx.processors;

public class BusinessObjectEventProcessor<T> {
    public void succeedWith(T event) {
    }

    public void failWith(Exception e) {
    }
}
