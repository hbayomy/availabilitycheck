package com.kites.businesslogic.services;

import com.kites.jpa.entities.BusinessObject;
import com.kites.tx.TransactionManagerAdapter;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class CodeGenerationService {

    private TransactionManagerAdapter transactionManager;

    public CodeGenerationService(TransactionManagerAdapter transactionManager){
        assert (transactionManager != null);
        this.transactionManager = transactionManager;
    }

    public <E extends BusinessObject>  String generateYearSerialCode(Class<E> entityClass, LocalDateTime time) {
        Integer year = time.getYear();
        String initialCode = new StringBuilder()
                .append(year.toString())
                .append("000000")
                .toString();
        Optional<Long> maxId = findMaxId(entityClass);
        Optional<String> codeOfPOWithMaxId =
                findCodeWithMaxIdAndStartsWithYear(entityClass,maxId.orElse(0L),year.toString());
        String poCode = codeOfPOWithMaxId.orElse(initialCode);
        year = Integer.valueOf(poCode.substring(0,4));
        Integer serial = Integer.valueOf(poCode.substring(4)) + 1;
        poCode = String.valueOf( (year * 1000000L)  + serial );
        return poCode;
    }

    public <E extends BusinessObject>  String generateSerialCode(Class<E> entityClass) {
        Optional<Long> maxId = findMaxId(entityClass);
        Optional<String> codeOfPOWithMaxId =
                findCodeWithMaxId(entityClass,maxId.orElse(0L));
        String poCode = codeOfPOWithMaxId.orElse("0") ;
        poCode = String.valueOf(Long.valueOf(poCode) + 1);
        StringBuilder codeBuilder = new StringBuilder(poCode);
        while((6 - codeBuilder.length())>0) codeBuilder.insert(0,"0");
        return codeBuilder.toString();
    }

    private <E extends BusinessObject> Optional<Long> findMaxId(Class<E> entityClass){
        String queryString = "SELECT MAX(E.id) FROM "+entityClass.getSimpleName()+" E ";
        Long maxId = this.<Long>executeQuery(queryString);
        return Optional.ofNullable(maxId);
    }

    private <E extends BusinessObject> Optional<String>
    findCodeWithMaxId(Class<E> entityClass, Long maxId){
        String queryString =
                "SELECT E.code FROM "+entityClass.getSimpleName() +" E WHERE E.id = :maxId";
        QueryParameter maxIdParam = new QueryParameter("maxId",maxId);
        String code = this.<String>executeQuery(queryString, maxIdParam);
        return Optional.ofNullable(code);
    }

    private <E extends BusinessObject> Optional<String>
    findCodeWithMaxIdAndStartsWithYear(Class<E> entityClass, Long maxId, String year){
        String queryString =
                "SELECT E.code FROM "+entityClass.getSimpleName()
                        +" E WHERE E.id = :maxId AND E.code like concat(:year,'%')";
        QueryParameter maxIdParam = new QueryParameter("maxId",maxId);
        QueryParameter yearParam = new QueryParameter("year",year);
        String code = this.<String>executeQuery(queryString, maxIdParam, yearParam);
        return Optional.ofNullable(code);
    }

    private <R> R executeQuery(String queryString, QueryParameter...params) {
        EntityManager entityManager = transactionManager.getEntityManager();
        Query query = entityManager.createQuery(queryString);
        for (QueryParameter p: params) {
            query.setParameter(p.name,p.value);
        }
        List result = query.getResultList();
        R ret = (result==null || result.isEmpty())? null : (R) result.get(0);
        entityManager.close();
        return ret;
    }

    private class QueryParameter{
        String name;
        Object value;
        QueryParameter(String name, Object value){
            this.name = name;
            this.value = value;
        }
    }
}
