package com.kites.businesslogic.services;

import com.kites.jpa.entities.md.items.Item;
import com.kites.tx.TransactionManagerAdapter;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

public class ItemService {

    private TransactionManagerAdapter transactionManager;
    private CodeGenerationService codeGenerator;

    public ItemService(TransactionManagerAdapter transactionManager){
        this.transactionManager = transactionManager;
        assert (this.transactionManager!=null);
        this.codeGenerator = new CodeGenerationService(transactionManager);
    }

    public Item createNewItem(String itemName, Long itemGroupId, List<Long> purchaseUnits, Long baseUnitId)
    throws Exception {
        Item item = new Item();
        LocalDateTime time = LocalDateTime.now();
        item.setCreationDate(Timestamp.valueOf(time));
        item.setCreationInfo("Hi, Item is created");
        item.setModificationDate(Timestamp.valueOf(time));
        item.setModificationInfo("Hi, Item is created");
        item.setName(itemName);
        item.setItemGroupId(itemGroupId);
        item.setCode(codeGenerator.generateSerialCode(Item.class));
        //item.setPurchaseUnits(new ArrayList<Long>(purchaseUnits));
        item.setBaseUnitId(baseUnitId);
        return transactionManager.doTransaction((EntityManager entityManager) -> {
            entityManager.persist(item);
            return item;
        });
    }

    public void deleteAllItems() throws Exception {
        transactionManager.doTransaction((EntityManager entityManager)->{
            Query delete_all_items = entityManager.createQuery("DELETE FROM Item");
            delete_all_items.executeUpdate();
            return null;
        });
    }
}
