package com.kites.businesslogic.services;

import com.kites.jpa.entities.tr.po.PurchaseOrder;
import com.kites.jpa.entities.tr.po.PurchaseOrderItem;
import com.kites.jpa.entities.tr.po.PurchaseOrderItemQuantity;
import com.kites.jpa.entities.tr.po.PurchaseOrderType;
import com.kites.businesslogic.commands.PurchaseOrderCreateCommand;
import com.kites.businesslogic.commands.PurchaseOrderCreationData;
import com.kites.tx.TransactionManagerAdapter;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PurchaseOrderService {

    private TransactionManagerAdapter transactionManager;
    private CodeGenerationService codeGenerator;

    public PurchaseOrderService(
            TransactionManagerAdapter transactionManager) {
        this.transactionManager = transactionManager;
        assert (this.transactionManager != null );
        this.codeGenerator = new CodeGenerationService(transactionManager);
    }

    public PurchaseOrder createImportPurchaseOrder(Long vendorId, Long purchaseUnitId,
                                                   Long purchaseResponsibleId) throws Exception {
        PurchaseOrderCreateCommand createCommand = new PurchaseOrderCreateCommand(transactionManager);
        PurchaseOrderCreationData importPO = new PurchaseOrderCreationData(String.valueOf(PurchaseOrderType.IMPORT),
                vendorId, purchaseUnitId, purchaseResponsibleId);
        return createCommand.execute(importPO);
    }

    public PurchaseOrder createLocalPurchaseOrder(Long vendorId, Long purchaseUnitId,
                                                  Long purchaseResponsibleId) throws Exception {
        PurchaseOrderCreateCommand createCommand = new PurchaseOrderCreateCommand(transactionManager);
        PurchaseOrderCreationData localPO = new PurchaseOrderCreationData(String.valueOf(PurchaseOrderType.LOCAL),
                vendorId, purchaseUnitId, purchaseResponsibleId);
        return createCommand.execute(localPO);
    }

    public PurchaseOrder addOrderItem(Long orderId, Long itemId) throws Exception {
        return transactionManager.doTransaction((EntityManager entityManager) -> {
            PurchaseOrder purchaseOrder = entityManager.find(PurchaseOrder.class,orderId);
            addOrderItem(purchaseOrder,itemId);
            return purchaseOrder;
        });
    }

    public PurchaseOrder addItemQuantity(Long orderId, Long itemId, Double qnty,
                                         Long unitOfMeasureId, Double price, Double discount) throws Exception {
        return transactionManager.doTransaction((EntityManager entityManager) -> {
            PurchaseOrder purchaseOrder = entityManager.find(PurchaseOrder.class,orderId);
            Optional<PurchaseOrderItem> itemOption = findOrderItemWithItemId(purchaseOrder,itemId);
            PurchaseOrderItem item = itemOption.get();
            addQuantity(item,qnty,unitOfMeasureId,price,discount);
            return purchaseOrder;
        });
    }


    private void addOrderItem(PurchaseOrder purchaseOrder, Long itemId) {
        PurchaseOrderItem orderItem = new PurchaseOrderItem();
        orderItem.setItemId(itemId);
        orderItem.setPurchaseOrderId(purchaseOrder.getId());
        Optional<List<PurchaseOrderItem>> initItemsList =
                Optional.ofNullable(purchaseOrder.getOrderItems());
        List<PurchaseOrderItem> items = initItemsList.orElse(new ArrayList<>());
        items.add(orderItem);
        purchaseOrder.setOrderItems(items);
    }

    private Optional<PurchaseOrderItem>
    findOrderItemWithItemId(PurchaseOrder purchaseOrder, Long itemId){
        Optional<List<PurchaseOrderItem>> items =
                Optional.ofNullable(purchaseOrder.getOrderItems());
        PurchaseOrderItem foundItem = null;
        for(PurchaseOrderItem poItem : items.orElse(new ArrayList<>())) {
            if (poItem.getId().equals(itemId)){
                foundItem = poItem;
                break;
            }
        }
        return Optional.ofNullable(foundItem);
    }

    private void addQuantity(PurchaseOrderItem purchaseOrderItem, Double qnty, Long unitOfMeasureId, Double price, Double discount) {
        Optional<List<PurchaseOrderItemQuantity>> intiQuantityList =
                Optional.ofNullable(purchaseOrderItem.getItemQuantities());
        List<PurchaseOrderItemQuantity> itemQuantities =
                intiQuantityList.orElse(new ArrayList<>());
        PurchaseOrderItemQuantity quantity = new PurchaseOrderItemQuantity();
        quantity.setOrderItemId(purchaseOrderItem.getId());
        quantity.setQuantity(qnty);
        quantity.setUnitOfMeasureId(unitOfMeasureId);
        quantity.setPrice(price);
        quantity.setDiscount(discount);
        itemQuantities.add(quantity);
        purchaseOrderItem.setItemQuantities(itemQuantities);
    }

    public void deleteAllPurchaseOrders() throws Exception {
        transactionManager.doTransaction((EntityManager entityManager)->{
            Query delete_all_purchaseOrders = entityManager.createQuery("DELETE FROM PurchaseOrder ");
            delete_all_purchaseOrders.executeUpdate();
            return null;
        });
    }
}
