package com.kites;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class})
@Import({BeansConfiguration.class})
@EntityScan({"com.kites.jpa.entities","com.kites.jpa.entities.md",
        "com.kites.jpa.entities.md.items","com.kites.jpa.entities.md.vendors",
        "com.kites.jpa.entities.tr","com.kites.jpa.entities.tr.po"})
@EnableJpaRepositories(basePackages = {"com.kites.jpa.repositories"})
public class JPATestApplication {

    private static Logger LOG = LoggerFactory.getLogger(JPATestApplication.class);

    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(JPATestApplication.class,args);
        LOG.info("APPLICATION FINISHED");
    }

}
