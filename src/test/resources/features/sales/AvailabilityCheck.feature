Feature: Request To Check Item Availability

  The sales man requires to ask the system about the item quantities shall be available at a certain date
  Remember that it is an NP problem ;) but we will tackle it gradually :) using constraint satisfaction planning

  Background: Available Stock in Store Houses

    Given the following users are exist and have the following roles:
      | User  | Role      |
      | Ahmed | Sales Man |
    And the following roles has permissions:
      | Role      | Permission                              |
      | Sales Man | StockTransaction:CheckAvailableQuantity |

    Given the ALL items quantities are exist in StockLedger as follows:
      | Store        | Item      | Quantity | Cost  | EntryDate  | StockType    |
      | Maadi Store  | Black-Ink | 5        | 155.5 | 12-05-2019 | Unrestricted |
      | Maadi Store  | Blue-Ink  | 1        | 12.00 | 20-07-2019 | Reserved     |
      | Helwan Store | Black-Ink | 2        | 160.4 | 20-07-2019 | Unrestricted |
    And the Transportation cost is as follows:
      | Store        | Customer    | Cost |
      | Maadi Store  | Ahram Press | 55   |
      | Helwan Store | Ahram Press | 60   |


  Scenario: (01) The sales man checks the item availability successfully
    Given the user is logged in as "Ahmed"
    When the user "Ahmed" checks availability for customer "Ahram Press" about item with code "0022" at date "05 Feb 2020"
    Then the following quantities are returned:
      | Store       | Item     | Quantity | Cost  | TotalCost | Sequence |
      | Maadi Store | Black-In | 5        | 166.5 | 832.5     | 1        |
      | Maadi Store | Black-In | 2        | 190.4 | 180.8     | 2        |


