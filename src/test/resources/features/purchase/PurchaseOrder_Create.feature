Feature: Create Purchase Order Successfully

  Background:

    Given the following users are exist and have the following roles:
      | userId | user  | role                 |
      | 1      | Gehan | Purchase Responsible |
    And the following roles has permissions:
      | Role                 | Permission           |
      | Purchase Responsible | PurchaseOrder:Create |

    Given the following BusinessUnits are exist:
      | businessUnitId | businessUnitName |
      | 1              | Textile          |
    And the following Vendors are exist:
      | vendorId | vendorName  |
      | 1        | Ahram Press |


  Scenario: (1)  Authorized user creates PurchaseOrder Successfully
    Given the user is logged in as "Gehan"
    When the user "Gehan" requests to create PurchaseOrder with te following:
      | orderType | vendorId | purchaseUnitId | purchaseResponsibleId |
      | IMPORT    | 1        | 1              | 1                     |
    Then the success message "The Purchase Order is created successfully" is displayed to user