Feature: Add Items in Order

  Background:

    Given the following users are exist and have the following roles:
      | userId | user  | role                 |
      | 1      | Gehan | Purchase Responsible |
    And the following roles has permissions:
      | Role                 | Permission           |
      | Purchase Responsible | PurchaseOrder:Create |

    Given the following items are exists:
      | itemId | itemName  |
      | 1      | White Ink |
    And the following orders are exists:
      | orderId |
      | 1       |

  Scenario: (01) Add item in order successfully
    Given the user is logged in as "Gehan"
    When the user "Gehan" requests to add item with id "1" to purchase order with id "1"
    Then the success message "Item is added successfully" is displayed to user