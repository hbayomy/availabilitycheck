package com.kites.testrunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/features/purchase/PurchaseOrder_Create.feature"},
    glue = {"com.kites.steps.useraccount","com.kites.steps.purchase","com.kites.steps.masterdata"})
public class MainRunner {
}
