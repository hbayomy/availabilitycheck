package com.kites.businesslogic.commands;

import com.kites.JPATestApplication;
import com.kites.jpa.entities.tr.po.PurchaseOrder;
import com.kites.jpa.repositories.PurchaseOrderRepository;
import com.kites.tx.DefaultTransactionManagerAdapter;
import com.kites.tx.TransactionManagerAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManagerFactory;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {JPATestApplication.class})
public class CreatePurchaseOrderTest implements InitializingBean {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    private TransactionManagerAdapter transactionManagerAdapter;
    private PurchaseOrderCreateCommand purchaseOrderCreateCommand;



    @Override
    public void afterPropertiesSet() throws Exception {
        transactionManagerAdapter =
                new DefaultTransactionManagerAdapter(entityManagerFactory);
    }

    @Before
    public void setup(){
        purchaseOrderCreateCommand =
                new PurchaseOrderCreateCommand(transactionManagerAdapter);
    }

    @Test
    public void createImportPurchaseOrder() throws Exception {
        PurchaseOrderCreationData importPOData =
                new PurchaseOrderCreationData("IMPORT",1L, 1L, 1L);
        PurchaseOrder importPurchaseOrder =  purchaseOrderCreateCommand.execute(importPOData);
        assertThatPurchaseOrderIsCreatedSuccessfully(importPurchaseOrder);
    }

    private void assertThatPurchaseOrderIsCreatedSuccessfully(PurchaseOrder purchaseOrder) {
        Optional<PurchaseOrder> purchaseOrderOptional = purchaseOrderRepository.findById(purchaseOrder.getId());
        assertTrue(purchaseOrderOptional.isPresent());
        assertEquals(purchaseOrder,purchaseOrderOptional.get());
    }
}
