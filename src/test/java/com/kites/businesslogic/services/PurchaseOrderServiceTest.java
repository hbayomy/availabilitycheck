package com.kites.businesslogic.services;

import com.kites.JPATestApplication;
import com.kites.jpa.entities.tr.po.PurchaseOrder;
import com.kites.jpa.entities.tr.po.PurchaseOrderType;
import com.kites.jpa.repositories.PurchaseOrderRepository;
import com.kites.tx.DefaultTransactionManagerAdapter;
import com.kites.tx.TransactionManagerAdapter;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {JPATestApplication.class})
public class PurchaseOrderServiceTest implements InitializingBean {

    @Autowired
    private EntityManagerFactory entityManagerFactory;
    private TransactionManagerAdapter transactionManagerAdapter;
    private PurchaseOrderService purchaseOrderService;
    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;


    @Override
    public void afterPropertiesSet(){
        transactionManagerAdapter =
                new DefaultTransactionManagerAdapter(entityManagerFactory);
        purchaseOrderService =
                new PurchaseOrderService(transactionManagerAdapter);
    }

    @After
    public void tearDown() throws Exception {
        purchaseOrderService.deleteAllPurchaseOrders();
    }

    @Test
    public void createImportPurchaseOrder() throws Exception {
        PurchaseOrder importPurchaseOrder =
                purchaseOrderService.createImportPurchaseOrder(1L, 1L, 1L);
        Optional<PurchaseOrder> purchaseOrder = purchaseOrderRepository.findById(importPurchaseOrder.getId());
        Optional<String> code = getLatestPurchaseOrderCodeForYear(String.valueOf(LocalDateTime.now().getYear()));
        assertTrue(purchaseOrder.isPresent());
        assertEquals(importPurchaseOrder.getCode(),code.get());
    }

    @Test
    public void createLocalPurchaseOrder() throws Exception {
        PurchaseOrder localPurchaseOrder =
                purchaseOrderService.createLocalPurchaseOrder(2L, 2L, 2L);
        Optional<PurchaseOrder> purchaseOrder = purchaseOrderRepository.findById(localPurchaseOrder.getId());
        Optional<String> code = getLatestPurchaseOrderCodeForYear(String.valueOf(LocalDateTime.now().getYear()));
        assertTrue(purchaseOrder.isPresent());
        assertEquals(localPurchaseOrder.getCode(),code.get());

    }

    @Test
    public void incrementPurchaseOrderCodeForCurrentYear() throws Exception{
        PurchaseOrder importPurchaseOrder =
                purchaseOrderService.createImportPurchaseOrder(1L, 1L, 1L);
         PurchaseOrder localPurchaseOrder =
                 purchaseOrderService.createLocalPurchaseOrder(2L, 2L, 2L);
         Integer year = LocalDateTime.now().getYear();
         String local_po_code = year + "000002";
         String import_po_code = year + "000001";
         assertEquals(local_po_code, localPurchaseOrder.getCode());
         assertEquals(import_po_code, importPurchaseOrder.getCode());
    }

    @Test
    public void incrementFirstPurchaseOrderCodeForCurrentYear() throws Exception{
        PurchaseOrder purchaseOrder = createPurchaseOrderForYear("IMPORT","2018");
        assertEquals(getLatestPurchaseOrderCodeForYear("2018").get(),"2018000006");
        PurchaseOrder localPurchaseOrder =
                purchaseOrderService.createLocalPurchaseOrder(2L, 2L, 2L);
        Integer year = LocalDateTime.now().getYear();
        String local_po_code = year + "000001";
        assertEquals(local_po_code, localPurchaseOrder.getCode());
    }

    @Test
    public void addOrderItemsToImportPurchaserOrder() throws Exception {
        PurchaseOrder importPurchaseOrder =
                purchaseOrderService.createImportPurchaseOrder(1L, 1L, 1L);
        importPurchaseOrder = purchaseOrderService.addOrderItem(importPurchaseOrder.getId(),1L);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.getOne(importPurchaseOrder.getId());
        assertEquals(importPurchaseOrder.getOrderItems(),purchaseOrder.getOrderItems());
    }


    @Test
    public void addItemQuantityToImportPurchaseOrder() throws Exception {
        PurchaseOrder importPurchaseOrder =
                purchaseOrderService.createImportPurchaseOrder(1L, 1L, 1L);
        purchaseOrderService.addOrderItem(importPurchaseOrder.getId(),1L);
        importPurchaseOrder = purchaseOrderService
                .addItemQuantity(importPurchaseOrder.getId(),1L,25.4, 1L, 1000.01, 0.15);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.getOne(importPurchaseOrder.getId());
        assertEquals(importPurchaseOrder.getOrderItems(),purchaseOrder.getOrderItems());
    }

    private Optional<String> getLatestPurchaseOrderCodeForYear(String year) {
        Optional<Long> maxId = purchaseOrderRepository.findMaxId();
        return purchaseOrderRepository.findPurchaseOrderCodeWithMaxIdAndStartsWithYear(maxId.orElse(0L),year);
    }

    private PurchaseOrder createPurchaseOrderForYear(String orderType,String year){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        LocalDateTime time = LocalDateTime.now();
        purchaseOrder.setCreationDate(Timestamp.valueOf(time));
        purchaseOrder.setCreationInfo("Hi, PO is created");
        purchaseOrder.setModificationDate(Timestamp.valueOf(time));
        purchaseOrder.setModificationInfo("Hi, PO is created");
        purchaseOrder.setCurrentStates("[Draft]");
        purchaseOrder.setOrderType(PurchaseOrderType.valueOf(orderType));
        purchaseOrder.setVendorId(3L);
        purchaseOrder.setPurchaseUnitId(3L);
        purchaseOrder.setPurchaseResponsibleId(3L);
        purchaseOrder.setCode(year+"000006");
        purchaseOrder.setVersion("v1.0");
        entityManager.persist(purchaseOrder);
        transaction.commit();
        entityManager.close();
        return purchaseOrder;
    }

}