package com.kites.businesslogic.services;

import com.kites.JPATestApplication;
import com.kites.jpa.entities.md.items.Item;
import com.kites.jpa.repositories.ItemRepository;
import com.kites.tx.DefaultTransactionManagerAdapter;
import com.kites.tx.TransactionManagerAdapter;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManagerFactory;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {JPATestApplication.class})
public class ItemServiceTest implements InitializingBean {

    @Autowired
    private EntityManagerFactory entityManagerFactory;
    private TransactionManagerAdapter transactionManagerAdapter;
    private ItemService itemService;
    @Autowired
    private ItemRepository itemRepository;

    @Override
    public void afterPropertiesSet() throws Exception {
        transactionManagerAdapter = new DefaultTransactionManagerAdapter(entityManagerFactory);
        itemService = new ItemService(transactionManagerAdapter);
    }

    @After
    public void tearDown() throws Exception {
        itemService.deleteAllItems();
    }

    @Test
    public void createNewItem() throws Exception {
        List<Long> purchaseUnits = Arrays.asList(new Long[]{1L,2L,5L});
        Item item = itemService.createNewItem("item#1",1L,purchaseUnits,4L);
        Item itemRetrieved = itemRepository.getOne(item.getId());
        assertEquals(item,itemRetrieved);
    }

    @Test
    public void createTowItems() throws Exception {
        List<Long> purchaseUnits = Arrays.asList(new Long[]{1L,2L,5L});
        Item item1 = itemService.createNewItem("item#1",1L,purchaseUnits,4L);
        Item item2 = itemService.createNewItem("item#2",1L,purchaseUnits,4L);
        assertEquals("000001", item1.getCode());
        assertEquals("000002", item2.getCode());
    }
}

