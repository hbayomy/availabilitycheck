package com.kites.businesslogic.rx;

import com.kites.jpa.entities.tr.gr.GoodsReciept;
import com.kites.businesslogic.rx.events.GRPostedEvent;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class BusinessObjectChannelTest {

    private BusinessObjectChannel channel;

    @Before
    public void setup(){
        channel = BusinessObjectChannel.getInstance();
    }

    @Test
    public void postGLTransactionsAfterPostingGR(){
        GRPostedEvent postGREvent =
                new GRPostedEvent(1L,"20190000001", 35500.2, 3L,
                        "000003", "Available", 2L, "000002");
        final List<GRPostedEvent> raisedEvents = new ArrayList<>();
        channel.subscribe( e->{ raisedEvents.add((GRPostedEvent) e); }, x->{} , GoodsReciept.class);
        channel.publish(postGREvent);
        assertThat(raisedEvents, hasItem(postGREvent));
    }


}
