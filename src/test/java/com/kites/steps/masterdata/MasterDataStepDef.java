package com.kites.steps.masterdata;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;

public class MasterDataStepDef {
    @Given("the following BusinessUnits are exist:")
    public void theFollowingBusinessUnitsAreExist(DataTable businessUnits) {
    }

    @And("the following Vendors are exist:")
    public void theFollowingVendorsAreExist(DataTable vendors) {
    }

    @Given("the following items are exists:")
    public void theFollowingItemsAreExists(DataTable items) {
    }
}
