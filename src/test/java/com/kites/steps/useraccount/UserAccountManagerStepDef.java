package com.kites.steps.useraccount;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;

public class UserAccountManagerStepDef {

    @Given("^the following users are exist and have the following roles:$")
    public void checkThatUsersAreExistAndHaveRoles(DataTable users) {

    }

    @And("^the following roles has permissions:$")
    public void checkThatRolesHavePermissions(DataTable roles) {

    }

    @Given("the user is logged in as {string}")
    public void theUserIsLoggedInAs(String user) {
    }
}
