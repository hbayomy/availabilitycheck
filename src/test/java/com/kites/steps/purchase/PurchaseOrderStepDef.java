package com.kites.steps.purchase;

import com.kites.JPATestApplication;
import gherkin.deps.com.google.gson.JsonObject;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

import static io.restassured.RestAssured.given;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
        classes = {JPATestApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PurchaseOrderStepDef {
    private String actualMessage;

    @Then("the success message {string} is displayed to user")
    public void theSuccessMessageIsDisplayedToUser(String expectedMessage) {
        Assert.assertEquals(expectedMessage,actualMessage);
    }

    @When("the user {string} requests to create PurchaseOrder with te following:")
    public void theUserRequestsToCreatePurchaseOrderWithTeFollowing(String user, DataTable purchaseOrderCreationData) {
        JsonObject po = new JsonObject();
        for(Map<String,String> param:purchaseOrderCreationData.asMaps()){
            param.forEach(
                    (key,value)->{po.addProperty(key,value);}
            );
        }
        Response post = given().contentType(ContentType.JSON).body(po.toString()).post("purchase-order/create");
        actualMessage = post.asString();
    }

    @And("the following orders are exists:")
    public void theFollowingOrdersAreExists(DataTable orders) {
    }
}
